<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = "books";
    protected $fillable = ['paid'];

    public function category(){
    	return $this->hasOne('App\Category','id','category_id');
    }

    public function invoice(){
    	return $this->hasOne('App\InvoiceM');
    }
}
