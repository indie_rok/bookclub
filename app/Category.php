<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = "categories";

	protected $fillable = ['name','active','index','price'];

	public function subs(){
		return $this->hasMany('App\Sub');
	}

	public function books(){
		return $this->belongsToMany('App\Book');
	}
}
