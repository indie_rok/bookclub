<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Book;
use Carbon\Carbon;
use Log;
use Request;
use Config;

class Blaster extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'blaster';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'sends emails';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

		$users = User::all();
		$blaster = [];
		$sendedBooksId = [];

		foreach ($users as $user) {
			$subsArray = [];
			$user = User::find($user->id);
			$subs = $user->subs;
			foreach ($subs as $sub) {
				$booksToSend = Book::where('paid','1')->where('sended','0')
														->where('aproved','1')
														->where('category_id',$sub->category_id)
														->where('sendDate',Carbon::today())
														->get();
				
				foreach ($booksToSend as $book){
					
					array_push($subsArray,[	'id' => $book->id,
											'title' => $book->title,
										    'price' => $book->price,
											'author' => $book->authorName,
											'amazonAsin' => $book->amazonAsin,
											'category' => $book->category->name,
											'coverExt' => $book->coverExt,
											'description' => $book->description
										]);
						array_push($sendedBooksId,$book->id);
				}
				
			}
			
			//if there are books for category
			if(count($subsArray)>0){
				array_push($blaster,[$user->email,$subsArray]);
			}
	
		}
		
		blaster($blaster);
		$sendedBooksId = array_unique($sendedBooksId);
		
		foreach ($sendedBooksId as $id) {
			$book = Book::find($id);
			$book->sended = 1;
			$book->save();
		}
		
		Log::info(print_r($blaster));
		Log::info(print_r($blaster,true));
		$this->comment('Done!!');
	}
}