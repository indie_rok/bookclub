<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Log;
use App\User;

class Inspire extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'inspire';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Display an inspiring quote';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		//User::create(['email'=>rand(0,99999)]);
		Log::info(PHP_EOL.Inspiring::quote().PHP_EOL);
		$this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
	}

}
