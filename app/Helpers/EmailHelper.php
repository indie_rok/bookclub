<?php

function sendEmail($recivers,$content){
	try{
		foreach ($recivers as $reciver) {
			Mail::send($content['view'], $content , function($message) use ($content,$reciver){
                    $message->to($reciver);
                    $message->subject($content['subject']);
            });
		}
	}
	catch(Exception $e){
		return redirect('pages.error',['error' => $e->getMessage()] );
	}
}

function blaster($data){
	try{
		for($i = 0;$i<count($data);$i++){
		$user = $data[$i][0];
		Mail::send('emails.blaster', ['bookInfo' => $data[$i][1] ], function($message) use ($user){
                    $message->to($user);
                    $message->subject('Check Out This Awesome Books!');
            });
	}
	}
	catch(Exception $e){
		return redirect('pages.error',['error' => $e->getMessage()] );
	}
	
	return 'test';
}

function getFileExtensionFromFile($path,$fileNameWithOutExtension){
	
	$posibleExtensions = ['jpg','jpeg','bmp','png'];
	
	foreach ($posibleExtensions as $ext) {
		$imgPath = $path.'/'.$fileNameWithOutExtension.'.'.$ext;
		
		$urlExists = get_headers($imgPath);
		$fileExists = stripos($urlExists[0],"200 OK")?true:false;

		if($fileExists){
			return $ext;
		}
	}
	
	return 'noext';
}
?>