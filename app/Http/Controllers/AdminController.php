<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Aproval;
use App\Book;
use App\Send;
use App\PayPendient;
use App\Category;
use App\Sub;
use App\User;
use App\Option;
use File;
use Session;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Services\Blaster;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//load books to aprove
		$books = Book::where('aproved','0')->get();
		$data['books'] = $books;

		return view('admin.index',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function aprove($input){
		$book = Book::find($input);
		$book->aproved = true;
		
		sendEmail([$book->email],[
									'view' => 'emails.aprove',
									'subject' => 'Thanks for your submission to FreeNovels',
									'book' => $book
		]);
		
		$options = Option::first();

		if($book->price>0){
			if($options->chargePaid){
				$book->paid=false;
				$book->save();
				$controller = app()->make('App\Http\Controllers\InvoicesController');
				$arguments = [$book->id]; // you need to put the arguments that the "create" method requires in here
				return  app()->call([$controller, 'create'], $arguments);
			}
			else{
				Session::flash('msj','Aproved!');
				$book->paid = true;
			}
		}
		else{
			if($options->chargeFree){
				$book->paid=false;
				$book->save();
				$controller = app()->make('App\Http\Controllers\InvoicesController');
				$arguments = [$book->id]; // you need to put the arguments that the "create" method requires in here
				return  app()->call([$controller, 'create'], $arguments);
			}
			else{
				Session::flash('msj','Aproved!');
				$book->paid = true;
			}
		}
		
		$book->save();
		
		return redirect('admin');
	}

	public function reject($input){
		
		$book = Book::find($input);

		sendEmail([$book->email],[
									'view' => 'emails.reject',
									'subject' => 'Thanks for your submission but...',
									'book' => $book
			]);
		$book->delete();

		return redirect('admin');
		
	}

	public function payments(){
		$books = Book::where('price','>','0')->where('paid','1')->get();
		$data['books'] = $books;
		return view('admin.payments',$data);
	}
	
	public function options(){
		$options = Option::first();
		return view('admin.options')->with('options',$options);
	}

	public function updateOptions(Request $input){
		
		$options = Option::first();

					$options->update([
									'username'=>$input->username,
									'password'=>$input->password,
									'chargeAmount'=>$input->chargeAmount,
									'blasterNumber' => $input->blasterNumber,
									'paypal_account' => $input->paypal_account,
									'onlyFreeBooks' => $input->onlyFreeBooks,
									'chargeFree' => $input->chargeFree,
									'chargePaid' => $input->chargePaid
									
								]);

		Session::flash('msj','Settings Updated');
		return redirect('admin/options');
	}

	public function books($status='pending'){
		if($status == 'paid'){
			$query = Book::where('paid','1')->get();
		}

		elseif($status == 'pending'){
			$query = Book::where('paid','0')->where('aproved','1')->get();
		}

		elseif($status == 'sended'){
			$query = Book::where('sended','1')->get();
		}

		else{
			return "error";	
		}
		return view('admin.books')->with('status',$status)->with('books',$query);
	}

	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	 
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)

	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
