<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Book;
use App\Aproval;
use App\Category;
use App\Option;
use Carbon\Carbon;
use Validator;
use Session;
use Storage;
use Response;

use JsValidator;

class BooksController extends Controller
{
    protected $validationRulesCreate=[
                'cover' => 'required',
                'title' => 'required',
                'email' => 'required',
                'category' => 'required',
                'sendDate' => 'required',
                'amazonAsin' => 'required',
                'price' => 'required',
                'authorName' => 'required',
                'description' => 'required',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('books.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
     
    public function calendar($category_id){
        $notFreeDays = [];

        $startDate = Carbon::tomorrow();
        $endDate = Carbon::tomorrow()->addMonths(3);

        while ($startDate->lt($endDate)){
            $bookCount = Book::where('category_id',$category_id)->where('sendDate',$startDate)->get()->count();
            if($bookCount<3){
                array_push($notFreeDays,$startDate->format('n/j/Y'));
            }
            $startDate = $startDate->addDay();
        }
     
        return $notFreeDays; 
        
    }
     
    public function create()
    {
        $endDate = Carbon::tomorrow()->addMonths(3);
        $options = Option::first();
        $validationJS = JsValidator::make($this->validationRulesCreate);
        $categories = Category::where('active','1')->orderBy('index','asc')->get();
        return view('books.create')->with(['categories'=>$categories,'validationJS'=>$validationJS])->with('endDate',$endDate)->with('options',$options);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
            $image = array('image' => $request->file('cover'));
            $data = array('title'=> $request->title,
                            'sendDate' => $request->sendDate,
                            'category_id' => $request->category[0],
                            'price' => $request->price,
                            'email' => $request->email,
                            'authorName' => $request->authorName,
                            'amazonAsin' => $request->amazonAsin,
                            'description' => $request->description

            );
            
            $rulesData = array('title'=>'required',
                                'sendDate'=>'required',
                                'category_id' =>'required',
                                'price'=>'required',
                                'email'=>'required|email',
                                'authorName'=>'required',
                                'amazonAsin'=>'required',
                                'description'=>'required'
            );
            
            

            // Within the ruleset, make sure we let the validator know that this
            // file should be an image
            $rulesImage = array(
                'image' => 'image'
            );
            
            
            $validatorData = Validator::make($data, $rulesData);
        
            // Now pass the input and rules into the validator
            $validatorImage = Validator::make($image, $rulesImage);
        
            if ($validatorData->fails())
            {
                Session::flash('msj', $validatorData->messages());
                return redirect('books/create');
            }
        
            if ($validatorImage->fails())
            {
                Session::flash('msj', 'Error: The provided file was not an image');
                return redirect('books/create');
            }
            
            $numberOfBooksToSendOnAuthorDate = Book::where('sendDate',$request->sendDate)->where('category_id',$request->category[0])->get()->count();
            
            if($numberOfBooksToSendOnAuthorDate > 2){
                Session::flash('msj','This date is full, please choose another date to send your book');
                return redirect('books/create');
            }
            
        
            $book = new Book;
            $book->title = $request->title;
            $book->category_id = $request->category[0];
            $book->sendDate = $request->sendDate;
            $book->price = $request->price;
            $book->email = $request->email;
            $book->authorName = $request->authorName;
            $book->sended = false;
            $book->coverExt = $request->file('cover')->getClientOriginalExtension();;
            $book->aproved = false;
            $book->paid = false;
            $book->amazonAsin = $request->amazonAsin;
            $book->description = $request->description;
            
            $options = Option::first();

            if($request->price> 0){
                if($options->chargePaid){
                    $book->paid = 0;

                }
                else{
                    $book->paid = 1;
                }
            }
            else{
                
                if($options->chargeFree){
                    $book->paid = 0;

                }
                else{
                    $book->paid = 1;
                }
            }
            
            $book->save();

            $imageName = $book->id . '.' . 
            $request->file('cover')->getClientOriginalExtension();

            Storage::disk('s3')->put('uploads/' . $imageName, file_get_contents($request->file('cover')));
       
            return(redirect('pages/aproval_needed'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
