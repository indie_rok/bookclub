<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use Session;
use App\Option;
use Carbon\Carbon;

use Illuminate\Http\Request;

class CategoriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['activeCategory'] = Category::where('active','1')->orderBy('index','asc')->get();
		$data['unactiveCategory'] = Category::where('active','0')->orderBy('name','asc')->get();
		return view('admin.categories.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
			$options = Option::first();
			return view('admin.categories.create')->with('categoryPrice',$options->chargeAmount);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$categoryIndex = (Category::where('active','1')->count())+1;
		$category = Category::create([
											'name' => $request->name,
											'active' => (boolean)$request->active,
											'index' => $categoryIndex,
											'price' => $request->price
									]);
		Session::flash('msj','Created');
		return redirect('admin/categories');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::find($id);
		return view('admin.categories.edit',$category);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$category = Category::find($id);
		$category->name = $request->name;
		$category->price = $request->price;
		$category->save();
		Session::flash('msj','Edited');
		return redirect('admin/categories');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$category = Category::find($id);
		$category->delete();
		Session::flash('msj','Deleted');
		return redirect('admin/categories');
	}

	public function activate($id){
		$category = Category::find($id);
		$category->active = true;
		$category->save();
		Session::flash('msj','Activated');
		return redirect('admin/categories');
	}


	public function desactivate($id){
		$category = Category::find($id);
		$category->active = false;
		$category->save();
		Session::flash('msj','Desactivated');
		return redirect('admin/categories');
	}

	public function updateIndexes(Request $input){
		$newOrder = array_combine($input->catId, $input->index);

		foreach ($newOrder as $categoryId => $index) {
			$cat = Category::find($categoryId);
			$cat->index = $index;
			$cat->save();
		}

		Session::flash('msj','Order Updated');

		return redirect('admin/categories');
	}

}
