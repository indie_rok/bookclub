<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Illuminate\Http\Request;
use App\Book;
use App\InvoiceM;
use Session;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Invoice;
use PayPal\Api\MerchantInfo;
use PayPal\Api\BillingInfo;
use PayPal\Api\InvoiceItem;
use PayPal\Api\Phone;
use PayPal\Api\Address;
use PayPal\Api\Currency;
use PayPal\Api\PaymentTerm;
use PayPal\Api\ShippingInfo;
use PayPal\Api\InvoiceAddress;

class InvoicesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	private $_api_context;

    public function __construct()
    {
        // setup PayPal api context
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

	public function index()
	{
		

	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($bookId)
	{
		$book = Book::find($bookId);
		$price = $book->category->price;

		$invoice = new Invoice();
		$invoice
		    ->setMerchantInfo(new MerchantInfo())
		    ->setBillingInfo(array(new BillingInfo()))
		    ->setNote("Payment for book: ".$book->title)
		    ->setPaymentTerm(new PaymentTerm())
		    ->setShippingInfo(new ShippingInfo());
		$invoice->getMerchantInfo()
		    ->setEmail("art_rainbow@hotmail.com")
		    ->setbusinessName("Free Novels")
		    ->setPhone(new Phone())
		    ->setAddress(new Address());

		$invoice->getMerchantInfo()->getPhone()
		    ->setCountryCode("001")
		    ->setNationalNumber("5032141716");
		$invoice->getMerchantInfo()->getAddress()
		    ->setLine1("1234 Main St.")
		    ->setCity("Portland")
		    ->setState("OR")
		    ->setPostalCode("97217")
		    ->setCountryCode("US");

		$billing = $invoice->getBillingInfo();
		$billing[0]
		    ->setEmail($book->email);

		$items = array();
		$items[0] = new InvoiceItem();
		$items[0]
		    ->setName("Distribution fee for book: ". $book->title)
		    ->setQuantity(1)
		    ->setUnitPrice(new Currency());

		$items[0]->getUnitPrice()
		    ->setCurrency("USD")
		    ->setValue($price);

		$invoice->setItems($items);

		$invoice->getPaymentTerm()
		    ->setTermType("NET_45");

		#$invoice->setLogoUrl('https://www.paypalobjects.com/webstatic/i/logo/rebrand/ppcom.svg');
		$request = clone $invoice;

		try {

		    $invoice->create($this->_api_context);
		} catch (Exception $ex) {
		 	return $ex->getMessage();
		}


		try {

		    $sendStatus = $invoice->send($this->_api_context);
		} catch (Exception $ex) {
		    
		   	return $ex->getMessage();
		}

		InvoiceM::create(['book_id'=>$book->id,'code'=>$invoice->getId()]);

		Session::flash('msj','Invoice send to:'.$book->email);
		return redirect('admin');
	}

	public function pay(Request $input){
		InvoiceM::where('code',$input->invoice)->get()->first()->book->update(['paid'=>'1']);
		return "ok";
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
