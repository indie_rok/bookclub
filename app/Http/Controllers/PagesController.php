<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('pages/index');
    }

   	public function thanks_for_register(){
   		return view('pages.thanks_for_register');
   	}

    public function aproval_needed(){
      return view('pages.aproval_needed');
    }

    public function hello(){
      return view('pages.hello');
    }

    public function error(){
      return view('pages.error');
    }

}