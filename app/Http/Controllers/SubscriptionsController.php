<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Session;
use App\PreUser;
use App\User;
use App\Category;
use App\Token;
use App\Sub;
use Mail;
use App\Helpers\EmailHelper;

use JsValidator;
use Validator;


class SubscriptionsController extends Controller
{
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $activeCategories = Category::where('active','1')->orderBy('index','asc')->get();
        return view('subscriptions.create')->with(['activeCategories'=>$activeCategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $userExists = count(User::where('email','=',$request->email)->get());
        
        
        $validator = Validator::make(
            ['email'=> $request->email],
            ['email' => 'required|email|unique:users']
        );

        if($validator->fails()){
            Session::flash('msj', $validator->messages());
            return redirect('subscriptions/create');
        }
        else{
            $userNeedsToVerifyEmail = count(PreUser::where('email','=',$request->email)->get());

            if ($userNeedsToVerifyEmail) {
                return redirect('subscriptions/create')->with('msj', 'Verify your email please.');
                #Session::flash('msj','Verify your email please.');
            }
            else{

                $toTokenize =  $request->email . Carbon::now()->toDateTimeString();
            
                $preuser = new PreUser;
                $preuser->email = $request->email;
                $preuser->subscriptions_string = $request->suscriptions;
                $preuser->save();

                $token = new Token;
                $token->code = hash_hmac('ripemd160',$toTokenize,env('APP_KEY', 'secret'));
                $token->pre_user_id = $preuser->id;
                $token->save();

                $data['email'] = $request->email;
                $data['token'] = $token->code;

                Mail::send('emails.password', $data , function($message) use ($data){
                    $message->to($data['email']);
                    $message->subject('Welcome To Free Book Club');
                });

                return redirect('/pages/thanks_for_register');
            }
        }

    }

    public function confirmToken($token){
        $tokenExists = Token::where('code',$token)->get()->first();

        if($tokenExists){
            $user = User::create(['email'=> $tokenExists->pre_user->email]);
            $subs = $tokenExists->pre_user->subscriptions_string;
            $subs = explode(',',$subs);

            foreach ($subs as $sub) {
                $createSubRow = Sub::create(['user_id'=>$user->id,'category_id'=>$sub]);
            }
            
            $tokenExists->pre_user->delete();
            $tokenExists->delete();
            return redirect('/pages/hello');
        }
        else{
            #abort(404);
            return 'Token does not exists, or has been used already.';
        }
    }

    public function confirmTokenEdit($token){
        $tokenExists = Token::where('code',$token)->get()->first();

        if($tokenExists){
            $data['allCategories'] = Category::all();
            $data['token'] = $token; 
            return view('subscriptions/editCategories',$data);
            
        }
        else{
            #abort(404);
            return 'Token does not exists, or has been used already.';
        }
    }

    public function editSubscriptions(Request $request){


        $token = Token::where('code',$request->userToken)->get()->first();
        
        if($token){
            Sub::where('user_id',$token->user_id)->delete();
            $subs = explode(',',$request->suscriptions);

            foreach ($subs as $sub) {
                $createSubRow = Sub::create(['user_id'=>$token->user_id,'category_id'=>$sub]);
            }

            $token->delete();
            Session::flash('msj','Edited Categories');
            return redirect('pages/hello');
        }
        else{
            abort(404);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit()
    {
        return view('subscriptions.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $emailExists = User::where('email',$request->email)->get()->first();
        if($emailExists){

            $generatedToken = hash_hmac('ripemd160', $request->email . Carbon::tomorrow()->toDateTimeString() ,env('APP_KEY', 'secret'));
            $tokenExists = Token::where('code',$generatedToken)->get()->first();

            if($tokenExists){
                Session::flash('msj','An access token has been send already. Check your inbox');
                return redirect('subscriptions/edit');
            }

            else{
                $token = new Token;
                $token->code = $generatedToken;
                $token->user_id = $emailExists->id;
                $token->save();

                sendEmail([$request->email],
                        ['subject'=>'Edit Your FreeNovels.Club Categories',
                        'view'=>'emails.editCategories',
                        'token'=>$token->code]
                    );

                return view('pages/thanks_for_register');
            }
            
        }

        else{
            Session::flash('msj','The email does not exists. Create an account please.');
            return redirect('subscriptions/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
