<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Category;
use App\Sub;
use Session;

use Illuminate\Http\Request;

class UsersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function test(){
		User::create(['email' => 'yo@yo.com']);
	}

	public function index()
	{
		$users = User::all();
		$data['users'] = $users;
		$data['numberOfUsers'] = count($users);
		return view('admin.users.index',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$data['user'] = $user;
		$data['categories'] = Category::all();
		return view('admin.users.edit',$data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $input)
	{
		if(count($input->cat)>0){
			$categoriesToEdit = [];
			foreach ($input->cat as $key => $value) {
				array_push($categoriesToEdit, $value);
			}
			
			$userCategories = Sub::where('user_id',$input->user_id)->delete();
			
			foreach ($categoriesToEdit as $key => $value) {
				Sub::create(['user_id'=>$input->user_id,'category_id'=>$value]);
			}

			Session::flash('msj','Categories Edited');
		}
		else{
			Session::flash('msj','You need to select at least one category');
		}
		
		return redirect('admin/users/'.$input->user_id.'/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::where('id',$id)->delete();
		Sub::where('user_id',$id)->delete();
		Session::flash('msj','Used deleted');
		return redirect('admin/users');
	}

}
