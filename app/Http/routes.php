<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','PagesController@index');

Route::get('subscriptions/edit','SubscriptionsController@edit');
Route::get('subscriptions/create','SubscriptionsController@create');
Route::post('subscriptions/store','SubscriptionsController@store');
Route::get('subscriptions/verify/new/{token}', 'SubscriptionsController@confirmToken');
Route::post('subscriptions/update','SubscriptionsController@update');
Route::get('subscriptions/verify/edit/{token}', 'SubscriptionsController@confirmTokenEdit');
Route::post('subscriptions/editSubscriptions','SubscriptionsController@editSubscriptions');

Route::get('pages/thanks_for_register','PagesController@thanks_for_register');
Route::get('pages/aproval_needed','PagesController@aproval_needed');
Route::get('pages/hello','PagesController@hello');
Route::get('pages/error','PagesController@error');

Route::get('admin/','AdminController@index');
Route::get('admin/aprove/{id}','AdminController@aprove');
Route::get('admin/reject/{id}','AdminController@reject');
Route::patch('admin/categories/updateIndexes','CategoriesController@updateIndexes');
Route::resource('admin/categories', 'CategoriesController');
Route::get('admin/categories/{id}/desactivate','CategoriesController@desactivate');
Route::get('admin/categories/{id}/activate','CategoriesController@activate');
Route::get('admin/payments','AdminController@payments');
Route::resource('admin/users', 'UsersController');
Route::get('admin/options','AdminController@options');
Route::post('admin/updateOptions','AdminController@updateOptions');
Route::get('admin/books','AdminController@books');
Route::get('admin/books/{status}','AdminController@books');
Route::get('admin/blaster','AdminController@blaster');
Route::get('admin/testEmail','AdminController@testEmail');

Route::get('books/create','BooksController@create');
Route::post('books/store','BooksController@store');
Route::get('books/index','BooksController@index');
Route::get('books/calendar/{category_id}','BooksController@calendar');

Route::get('invoices/create/{id}','InvoicesController@create');
Route::post('/invoices/pay','InvoicesController@pay');