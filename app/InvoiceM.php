<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceM extends Model {

	protected $table = 'invoices';
	protected $fillable = ['book_id','code'];

	public function book(){
		return $this->belongsTo('App\Book');
	}

}
