<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model {

	protected $table = 'options';
	protected $fillable = ['username',
							'password',
							'chargeAmount',
							'blasterNumber',
							'paypal_account',
							'onlyFreeBooks',
							'chargeFree',
							'chargePaid'
					];
}
