<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreUser extends Model
{
    protected $table = "pre_users";

    public function token(){
    	return $this->hasOne('App\Token');
    }
}
