<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Send extends Model {

	protected $table = 'sends';

	public function book(){
		return $this->belongsTo('App\Book');
	}

}
