<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = "tokens";

    public function pre_user(){
    	return $this->belongsTo('App\PreUser');
    }
}