<?php
return array(
    // set your paypal credential
    'client_id' => 'ATGnfgGdv7WeE5y25GBDO2HdeR902nS5vdtlAsY8UCk4YZ6DdIVsbyh_CLB0bj4E59ShIFzUB79k3AYz',
    'secret' => 'EBsPHexyKBdcgIHT1S0V--Y4F-e6CZFYxjm8c9p-TFP19rn0hWMopCQ61hgIgY_SplKAIe0iKfFrj4lY',

    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'live',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);