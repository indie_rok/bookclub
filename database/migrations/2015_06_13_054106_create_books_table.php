<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->date('sendDate');
            $table->string('coverExt');
            $table->float('price');
            $table->string('email');
            $table->string('description');
            $table->string('authorName');
            $table->boolean('sended');
            $table->boolean('paid');
            $table->boolean('aproved');
            $table->string('amazonAsin');
            $table->integer('category_id');
            $table->integer('invoice_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
