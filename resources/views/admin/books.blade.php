@extends('templates.admin')

@section('content')

<section class="row">
<br>
	<div class="btn-group" role="group" aria-label="...">
	  <a href="/admin/books/" type="button" class="btn btn-default text-center">Pending</a>
	  <a href="/admin/books/paid" class="btn btn-default text-center">Paid</a>
	  <a href="/admin/books/sended" class="btn btn-default text-center">Sended</a>
	</div>
</section>

<h1>{{$status}} books</h1>

@if (count($books) == 0)
	<h2 class="text-center alert alert-warning">No books yet...</h2>
@else
	<table class="table table-hover">
      <tbody>
      <thead>
      	<tr>
	      	<td class="col-md-2">Book Name</td>
	      	<td class="col-md-1">Price</td>
	      	<td class="col-md-2">Date</td>
      	</tr>
      </thead>

		@foreach ($books as $book)
	        <tr>
				<td>{{$book->title}}</td>
				<td>{{$book->price}}</td>
				<td>{{$book->created_at}}</td>
	        </tr>
        @endforeach
      </tbody>
    </table>

@endif

@stop