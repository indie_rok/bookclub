@extends('templates.admin')

@section('content')

<h1>New Category</h1>
<hr>



{!!Form::open(['action' => 'CategoriesController@store','class'=>'form-inline'])!!}
  <div class="form-group">
  	{!!Form::label('Name','Category name')!!}
    {!!Form::text('name',null,['class' => 'form-control' , 'placeholder' => 'Name..'])!!}
  </div>

	<div class="form-group">
  		{!!Form::label('price','Price:')!!}
    	{!!Form::text('price',$categoryPrice,['class' => 'form-control','placeholder' => ' 0 for free..'])!!}
  	</div>

  <div class="form-group">
	<div class="radio">
	  <label><input type="radio" name="active" checked value="true">Active</label>
	</div>
	<div class="radio">
	  <label><input type="radio" name="active" value="0">Unactive</label>
	</div>
	
  </div><br>



{!!Form::submit('Create', ['class'=> 'btn btn-primary'])!!}

{!!Form::close()!!}

@stop