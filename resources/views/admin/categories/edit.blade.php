@extends('templates.admin')

@section('content')

<h1>Edit Category</h1>
<hr>

{!!Form::open(['action' => ['CategoriesController@update', $id],'class'=>'form-inline','method' => 'PATCH'])!!}
  <div class="form-group">
  	{!!Form::label('Name','Category name')!!}
    {!!Form::text('name',$name,['class' => 'form-control'])!!}
  </div>
  <div class="form-group">
  	{!!Form::label('price','Category price')!!}
    {!!Form::text('price',$price,['class' => 'form-control'])!!}
  </div>

{!!Form::submit('Update name', ['class'=> 'btn btn-primary'])!!}

{!!Form::close()!!}

@stop