@extends('templates.admin')

@section('content')

<h1>Current Categories</h1>

<section class="row">
	<a href="{{url('admin/categories/create')}}" class="btn btn-primary pull-right">New category</a>
</section>

<section class="row">
	<section class="col-md-6">
		<h2>Active</h2>
		<table class="table table-hover">
	      <tbody>
			
		  @if(count($activeCategory) == 0)
				<h3 class="text-center">No active categories</h3>
			@else
			{!!Form::open(['url' => 'admin/categories/updateIndexes','method' => 'PATCH','name'=>'editIndexes'])!!}
			{!!Form::submit('Update Order',['class'=>'btn btn-primary'])!!}
			<br>
			<br>
		    @foreach ($activeCategory as $category)
		    	{!!Form::hidden('catId[]',$category->id)!!}
				<tr>
					<td>{!! Form::text('index[]',$category->index,['class'=>'col-md-1 form-control']) !!}</td>
					<th class="col-md-4"><a href="categories/{{$category->id}}/edit">{{$category->name}}</a></th>
					<th class="col-md-1"><a href="categories/{{$category->id}}/desactivate" class="btn btn-warning">Desactivate</a></th>
					<th class="col-md-1">
					<button type="button" data-recordid="{{$category->id}}"  class="btn btn-danger delete-btn" data-token="{{ csrf_token() }}">Delete</button>
					</th>
				</tr>
			
			@endforeach
			{!!Form::close()!!}
		  @endif

	      </tbody>
	    </table>
	</section>



	<secion class="col-md-6">
		<h2>Unactive</h2>
		<table class="table table-hover">
	     <tbody>
			     
		  @if(count($unactiveCategory) == 0)
				<h3 class="text-center">No unactive categories</h3>
			@else
		    @foreach ($unactiveCategory as $category)
				<tr>
					<th class="col-md-5"><a href="categories/{{$category->id}}/edit">{{$category->name}}</a></th>
					<th class="col-md-1"><a href="categories/{{$category->id}}/activate" class="btn btn-success">Activate</a></th>
					<th class="col-md-1">
						{!! Form::open(['action' => ['CategoriesController@destroy', $category->id], 'method' => 'delete']) !!}
  						{!! Form::submit('Delete', ['class'=>'btn btn-danger btn-mini']) !!}
						{!! Form::close() !!}
					</th>
				</tr>
			@endforeach
		  @endif

	     </tbody>
	    </table>
	</secion>

</section>

@stop

@section('scripts')
<script type="text/javascript">

 $('.delete-btn').on('click', function(){
        var recordID = $(this).data('recordid');
        var token = $(this).data('token');

        var ajax = $.ajax({
            url: "/admin/categories/" + recordID ,
            type: "POST",
            data: {_method: 'delete', _token :token},
            success:function(msg){
            	alert('deleted');
        		location.reload();
    		}
        })
    });
</script>

<script type="text/javascript">
	
</script>
@stop