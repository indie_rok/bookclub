@extends('templates.admin')

@section('content')

<h1>Books To Approve</h1>
<br>

@if (count($books) == 0)
	<h2 class="text-center alert alert-warning">No books yet..</h2>
@else
	<table class="table table-hover">
      <tbody>
      <thead>
      	<tr>
	      	<td class="col-md-2"></td>
	      	<td class="col-md-3">Title</td>
	      	<td class="col-md-2">Sign Up Date</td>
	      	<td class="col-md-2">Send Date</td>
	      	<td class="col-md-1">Author</td>
	      	<td class="col-md-1">Category</td>
	      	<td class="col-md-1">Price</td>
	      	<td class="col-md-1"></td>
	      	<td class="col-md-1"></td>
      	</tr>
      </thead>

		
		@foreach ($books as $key =>$book)
	        <tr>
	        <?php if($book->id == null){?>
	        		<td><img class="cover-admin" src="http://dummyimage.com/160x200/000/fff"/></td>
	        	<?php } else{ ?>
	        		<td><img class="cover-admin" src="https://s3.amazonaws.com/freenovels/uploads/{{$book->id}}.{{$book->coverExt}}"/></td>
	        		<?php } ?>
				<td><h2><a href="http://www.amazon.com/dp/{{$book->amazonAsin}}" target="_blank">{{$book->title}}</a></h2></td>
				<td>{{ date("d-M-Y",strtotime($book->created_at)) }}</td>
				<td>{{date("d-M-Y",strtotime($book->sendDate))}}</td>
				<td>{{$book->authorName}}</td>
				<td>{{$book->category->name}}</td>
				<td>{{$book->price}}</td>
				<td><a href="{{url('admin/aprove/'.$book->id)}}"><input type="button" class="btn btn-success" value="Aprove"></a></td>
				<td><a href="{{url('admin/reject/'.$book->id)}}"><input type="button" class="btn btn-danger" value="Delete"></a></td>
	        </tr>

        @endforeach
      </tbody>
    </table>

@endif

@stop