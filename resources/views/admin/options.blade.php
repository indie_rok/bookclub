@extends('templates.admin')

@section('content')

{!!Form::open(['url'=>'admin/updateOptions'])!!}

<h1>Login Info</h1>

{!!Form::label('Username')!!}
{!!Form::text('username',$options->username)!!}
{!!Form::label('Password')!!}
{!!Form::text('password',$options->password)!!}

<h1>Email Config</h1>

{!!Form::label('Charge Amount per book')!!}
{!!Form::text('chargeAmount',$options->chargeAmount)!!}

{!!Form::label('# of books paid to send blaster')!!}
{!!Form::text('blasterNumber',$options->blasterNumber)!!}

<hr>

{!!Form::label('Only Allow free books')!!}

@if($options->onlyFreeBooks)

    {!! Form::checkbox('onlyFreeBooks',1,true) !!}

@else
    {!!Form::checkbox('onlyFreeBooks',1,false)!!}
@endif


{!!Form::label('Charge for free books')!!}

@if($options->chargeFree)
    {!!Form::checkbox('chargeFree',1,true)!!}
@else
    {!!Form::checkbox('chargeFree',1,false)!!}

@endif


{!!Form::label('Charge for discounted books')!!}

@if($options->chargePaid)
    {!!Form::checkbox('chargePaid',1,true)!!}
@else
    {!!Form::checkbox('chargePaid',1,false)!!}
@endif




<h1>Paypal Config</h1>

{!!Form::label('Paypal Email')!!}
{!!Form::text('paypal_account',$options->paypal_account)!!}
<br><br>
{!!Form::submit('Update Settings',['class'=>'btn btn-primary'])!!}

{!!Form::close()!!}
@stop