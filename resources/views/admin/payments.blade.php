@extends('templates.admin')

@section('content')

<h1>Payments</h1>

@if (count($books) == 0)
	<h2 class="text-center alert alert-warning">No payments yet...</h2>
@else
	<table class="table table-hover">
      <tbody>
      <thead>
      	<tr>
	      	<td class="col-md-2">Paypal Code</td>
	      	<td class="col-md-2">Book Name</td>
	      	<td class="col-md-1">Price</td>
	      	<td class="col-md-2">Date</td>
      	</tr>
      </thead>

		
		@foreach ($books as $book)
	        <tr>
				<td>{{$book->invoice->code}}</td>
				<td>{{$book->title}}</td>
				<td>{{$book->price}}</td>
				<td>{{$book->invoice->updated_at}}</td>
	        </tr>

        @endforeach
      </tbody>
    </table>

@endif

@stop