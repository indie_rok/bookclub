@extends('templates.admin')

@section('content')

<h1>Edit User</h1>
<hr>

{!!Form::open(['url' => 'admin/users/'.$user->id,'class'=>'form-inline','method' => 'PATCH'])!!}
  <div class="form-group">
  	{!!Form::label('Email','Email')!!}
    {!!Form::text('email',$user->email,['class' => 'form-control'])!!}
	

	 <h2>Change Categories</h2>
	@foreach($categories as $cat)
		{!!Form::checkbox('cat[]',$cat->id)!!}
		{!!Form::label('categories',$cat->name)!!}
	@endforeach

	<br>

	{!!Form::hidden('user_id',$user->id)!!}

	{!!Form::submit('Update Settings', ['class'=> 'btn btn-primary'])!!}

  </div>

{!!Form::close()!!}

@stop