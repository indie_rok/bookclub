@extends('templates.admin')

@section('content')



<h1> <span class="alert alert-success">{{$numberOfUsers}}</span> registered Users</h1>

	<table class="table table-hover">
      <tbody>
      <thead>
      	<tr>
	      	<td class="col-md-2">Email</td>
	      	<td class="col-md-2">Subscription Date</td>
	      	<td class="col-md-2"></td>
      	</tr>
      </thead>

		
		@foreach ($users as $user)
	        <tr>
				<td><a href="users/{{$user->id}}/edit">{{$user->email}}</a></td>
				<td>{{$user->created_at}}</td>
				<td>
					{!!Form::open(['url'=>'admin/users/'.$user->id,'method'=>'delete'])!!}
					{!!Form::submit('Delete',[
												'class'=>'btn btn-danger'
											])!!}
					{!!Form::close()!!}
				</td>
	        </tr>

        @endforeach
      </tbody>
    </table>


@stop