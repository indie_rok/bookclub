@extends('templates.master')

@section('content')

<style type="text/css">
	.freeDay a{ background-color: #2ecc71 !important; color:white !important; }
	.notFreeDay span{background-color:#e74c3c !important;}
	.ui-datepicker-today span{
		background-color:#e74c3c !important;
	}
	
	#ajax-loader{
		display:none;
	}
	
	#calendar{
		cursor:pointer;
	}

</style>

  <div class="jumbotron" style="background: url('{{asset('img/writer.jpg')}}') no-repeat center center; background-size:cover; color:white">
      <div class="container">
        <h1 class="text-center space100px">Tell us about your awesome book</h1>
      </div>
  </div>

{!!Form::open(['action' => 'BooksController@store',
				'files' => true,
			]) !!}
{!! Form::token() !!}

  <article class="container">
	<section class=="row">

		<section class="col-md-2 col-md-offset-2">
			<section>
				<img src="http://dummyimage.com/160x200/000/fff" class="book-cover center-block"  id="screenshot">
				<section class="inputWrapper">
					Upload your cover
					{!!Form::file('chooseImage', array( 'onchange' => 'readURL(this)', 
														'class' => 'fileInput' , 
														'name' => 'cover')
													)!!}
				</section>
			</section>
		</section>
		<section class="col-md-6 col-md-offset-1">
			

				<section class="row">
					<label for="">Title</label>
					<input type="text" class="form-control width100" name="title">
				</section>

				<br>
				
				<section class="row">
					<label for="">Book Description</label>
					<textarea class="form-control width100" rows="3" name='description'></textarea>
				</section>
				
				<br>

				<section class="row">
					<section class="form-inline">
					  <div class="form-group  pull-left floatoverride">
					  	
					@if ($options->onlyFreeBooks)
					    <label for="exampleInputName2">Price</label>
					    <input type="text" class="form-control formAuthor" value='0' name="price" style="width:115px" readonly="readonly"  data-toggle="tooltip" data-placement="bottom" title='Right now we only accept free books.'>
					 
					@else
					    <label for="exampleInputName2">Price</label>
					    <input type="text" class="form-control formAuthor" placeholder="0 for free.." name="price" style="width:115px">
					@endif
						</div>
					</section>
					
					<section class="form-inline">
					  <div class="form-group  pull-right floatoverride">
					    <label>Your Name</label>
					    <input type="text" class="form-control formAuthor" name="authorName">
					  </div>
					</section>
				</section>

				<br>
				
				<section class="row">
					<section class="form-inline">
					  <div class="form-group pull-left floatoverride">
					    <label for="exampleInputName2">Category </label>
					    <select id='categories' class="form-control formAuthor" name="category[]">
						<option value="">---</option>
						@foreach($categories as $category)
						<option value="{{$category->id}}">{{$category->name}}</option>
						@endforeach

					    </select>
					  </div>
					  
					  <div class="form-group pull-right floatoverride">
					  	
					  	<label for="exampleInputEmail2">
					   		Send Date <img id='ajax-loader' src="{{ secure_asset('img/ajax-loader.gif')}}"></label>
					   <div class="input-group ">
					  	<input id="calendar"   class="form-control formAuthor" type='text' name="sendDate" placeholder='Select a category first..  ▼' readonly="readonly">
					  </div>
					  
					  </div>
						
						<section id="three_months_after" style='display:none'>{{$endDate}}</section>
					  
					</section>
				</section>
				
				
				
				<br>

				<section class="row">
					<section class="form-inline">
					  <div class="form-group  pull-left floatoverride">
					    <label for="exampleInputName2">Amazon ASIN</label>
					    <input type="text" class="form-control formAuthor ipadInputFix" name="amazonAsin" id="example" data-toggle="tooltip" data-placement="bottom" title="http://www.amazon.com/XXXXXX" style="width:135px">
					  </div>

					  <div class="form-group pull-right floatoverride">
					    <label for="exampleInputEmail2">Email</label>
					    <input type="text" class="form-control formAuthor ipadInputFix" id="exampleInputEmail2 " placeholder="jane.doe@example.com" name="email" style="width:145px">
					  </div>
					  
					</section>
				</section>


				
				<br>
				
				<section class="row"><input type="submit" class="btn btn-success center-block" value="Send My book"></section>

			
		</section>

	</section>
  </article>

{!!Form::close()!!}

@stop

@section('scripts')
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#screenshot').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})

	$('#categories').change(function(e){
		$('#calendar').val('');
		$('#ajax-loader').show();
		var selectedCategoryId = $("option:selected", this).val();
    	var valueSelected = this.value;
    	var homeUrl = window.location.protocol + "//" + window.location.host + "/";
    	
    	$.ajax({
    		url:homeUrl+'books/calendar/'+selectedCategoryId,
    	}).done(function( data ) {
		 $("#calendar").datepicker("destroy");
		 $('#ajax-loader').hide();
		 var datesArray=data;
		 var today = new Date();
		 var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
		 var threemonthsDate = new Date($('#three_months_after').text());

			  $(function(){
				$('#calendar').datepicker({
					 inline: true,
					 minDate:tomorrow,
					 dateFormat: 'yy-mm-dd',
					 maxDate:threemonthsDate,
					 beforeShowDay: function (date) {
				    	var theday =(date.getMonth()+1) +'/'+date.getDate()+ '/' +date.getFullYear();
				    	
						if($.inArray(theday, datesArray) >=0){
							return [true,"freeDay"];
						}
							else{
								return [false,'notFreeDay'];
							}
						},
				});
			});
		 	
		  });
	})


</script>

{!!$validationJS!!}
@stop