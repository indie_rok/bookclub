
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <!-- Facebook sharing information tags -->
        
        
        <title>*|MC:SUBJECT|*</title>
		
    <style type="text/css">
		img{
			display:block;
		}
		#outlook a{
			padding:0;
		}
		body{
			width:100% !important;
		}
		body{
			margin:0;
			padding:0;
		}
		img{
			border:none;
			outline:none;
			text-decoration:none;
		}
		body{
			background-image:url("http://gallery.mailchimp.com/0d61bb2ec9002f0e9872b8c36/images/bg_tiles.png");
			background-repeat:repeat;
			background-position:top left;
			background-color:#ECE7D5;
		}
		#container{
			background-image:url("http://gallery.mailchimp.com/0d61bb2ec9002f0e9872b8c36/images/bg_tiles.png");
			background-repeat:repeat;
			background-position:top left;
			background-color:#ECE7D5;
			height:100%;
			width:100%;
		}
	/*
	@tab Page
	  primary heading style
	@tip Primary headingss in your message body. Make them big and easy to read.
	@theme title
	*/
		.title{
			/*@editable*/color:#202020;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:46px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:75%;
			padding-bottom:10px;
		}
	/*
	@tab Page
	  secondary heading style
	@tip Secondary headings in your message body. Make these smaller than your primary titles, but bigger than your content text.
	@theme subtitle
	*/
		.subTitle{
			/*@editable*/color:#202020;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:24px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:75%;
			padding-bottom:10px;
		}
	/*
	@tab Header
	  preheader text style
	@tip The styling for your email's preheader text. Make it a color and size that is easy to read.
	*/
		#preheader{
			/*@editable*/color:#505050;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Header
	  preheader link style
	@tip The styling for your email's preheader hyperlinks. Make it a color that stands out from the rest of your text.
	*/
		#preheader a:link,#preheader a:visited{
			/*@editable*/color:#505050;
			/*@editable*/font-weight:bold;
			/*@editable*/text-decoration:underline;
		}
		#templateHeader{
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			-moz-box-shadow:0px 0px 5px #CCCCCC;
			-webkit-box-shadow:0px 0px 5px #CCCCCC;
			background-color:#FAF9F4;
			background-image:url("http://gallery.mailchimp.com/0d61bb2ec9002f0e9872b8c36/images/bg_header.png");
			background-repeat:repeat-x;
			background-position:bottom left;
			border:1px solid #CBC6B5;
			border-radius:5px;
			box-shadow:0px 0px 5px #CCCCCC;
		}
	/*
	@tab Header
	  text style
	@tip The styling for your email's header text. Make it a color and size that is easy to read.
	*/
		#templateHeader{
			/*@editable*/color:#252525;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:36px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:450%;
		}
	/*
	@tab Header
	  link style
	@tip The styling for your email's header hyperlinks. Make it a color that stands out from the rest of your text.
	*/
		#templateHeader a:link,#templateHeader a:visited{
			/*@editable*/color:#252525;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Header
	  text style
	@tip The styling for your email's header text. Make it a color and size that is easy to read.
	*/
		#headerImage{
			/*@editable*/text-align:center;
		}
		#headerImage img{
			max-width:530px;
		}
		#upperContent{
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			-moz-box-shadow:0px 0px 5px #CCCCCC;
			-webkit-box-shadow:0px 0px 5px #CCCCCC;
			background-color:#FAF9F4;
			background-image:url("http://gallery.mailchimp.com/0d61bb2ec9002f0e9872b8c36/images/bg_main.png");
			background-repeat:repeat-x;
			background-position:bottom left;
			border:1px solid #CBC6B5;
			border-radius:5px;
			box-shadow:0px 0px 5px #CCCCCC;
			text-align:left;
		}
		#upperContentImage img{
			display:inline;
			height:auto;
			max-width:250px;
		}
	/*
	@tab Body
	  upper content text style
	@tip The styling for your email's main content text. Make it a color and size that is easy to read.
	@theme main
	*/
		.upperContentCopy,.upperContentFileInfo{
			/*@editable*/color:#323232;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:14px;
			/*@editable*/line-height:180%;
		}
	/*
	@tab Body
	  upper content link style
	@tip The styling for your email's main content hyperlinks. Make them a color that stands out from the rest of your text.
	*/
		.upperContentCopy a:link,.upperContentCopy a:visited,.upperContentFileInfo a:link,.upperContentFileInfo a:visited{
			/*@editable*/color:#505050;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	  upper content button style
	@tip The styling for your email's main content button. Make it a color and size that draws attention.
	*/
		#upperContent .button{
			/*@tab Body
  upper content button style
@tip The styling for your email's main content button. Make it a color and size that draws attention.*/-moz-border-radius:5px;
			-webkit-border-radius:5px;
			-moz-box-shadow:0px 6px 10px #757575 inset;
			-webkit-box-shadow:0px 6px 10px #757575 inset;
			/*@editable*/background-color:#686868;
			border:2px solid #3F3F41;
			border-radius:5px;
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-size:16px;
			/*@editable*/font-family:Arial;
			/*@editable*/line-height:100%;
			text-align:center;
		}
	/*
	@tab Body
	  upper content button style
	@tip The styling for your email's main content button. Make it a color and size that draws attention.
	*/
		#upperContent .button a:link,#upperContent .button a:visited{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:bold;
			text-decoration:none;
			text-shadow:1px 2px 2px #303030;
		}
		#lowerContent{
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			-moz-box-shadow:0px 0px 5px #CCCCCC;
			-webkit-box-shadow:0px 0px 5px #CCCCCC;
			background-color:#FAF9F5;
			border:1px solid #CBC6B5;
			border-radius:5px;
			box-shadow:0px 0px 5px #CCCCCC;
			text-align:left;
		}
		.lowerContentImage{
			background-color:#EBE9E5;
			border:1px solid #D7D5C9;
			height:auto;
			max-width:135px;
			padding:5px;
		}
	/*
	@tab Body
	  lower content text style
	@tip The styling for your email's lower content text. Make it a color and size that is easy to read.
	@theme main
	*/
		.lowerContentCopy,.lowerContentFileInfo{
			/*@editable*/color:#323232;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
		}
	/*
	@tab Body
	  lower content link style
	@tip The styling for your email's lower content hyperlinks. Make them a color that stands out from the rest of your text.
	*/
		.lowerContentCopy a:link,.lowerContentCopy a:visited,.lowerContentFileInfo a:link,.lowerContentFileInfo a:visited{
			/*@editable*/color:#505050;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		.lowerContentCopy img{
			max-width:325px;
		}
	/*
	@tab Body
	  lower content button style
	@tip The styling for your email's lower content button. Make it a color and size that draws attention.
	*/
		#lowerContent .button{
			/*@tab Body
  lower content button style
@tip The styling for your email's lower content button. Make it a color and size that draws attention.*/-moz-border-radius:5px;
			-webkit-border-radius:5px;
			-moz-box-shadow:0px 6px 10px #757575 inset;
			-webkit-box-shadow:0px 6px 10px #757575 inset;
			/*@editable*/background-color:#686868;
			border:2px solid #3F3F41;
			border-radius:5px;
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-size:14px;
			/*@editable*/font-family:Helvetica;
			/*@editable*/line-height:100%;
			text-align:center;
		}
	/*
	@tab Body
	  lower content button style
	@tip The styling for your email's lower content button. Make it a color and size that draws attention.
	*/
		#lowerContent .button a:link,#lowerContent .button a:visited{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:bold;
			text-decoration:none;
			text-shadow:1px 2px 2px #303030;
		}
	/*
	@tab Footer
	  utility text style
	@tip The styling for your email's utility bar. Make it a color and size that is easy to read.
	*/
		#utility{
			/*@editable*/color:#202020;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:11px;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	  utility link style
	@tip The styling for your email's utility bar hyperlinks. Make them a color that stands out from the rest of your text.
	*/
		#utility a:link,#utility a:visited{
			/*@tab Footer
  utility link style
@tip The styling for your email's utility bar hyperlinks. Make them a color that stands out from the rest of your text.*/-moz-border-radius:5px;
			-webkit-border-radius:5px;
			background-color:#CAC6B5;
			border-radius:5px;
			/*@editable*/color:#202020;
			/*@editable*/font-weight:bold;
			margin:0 2px;
			padding:6px 12px;
			/*@editable*/text-decoration:none;
		}
		#utility img{
			border:none;
			display:inline;
			vertical-align:middle;
		}
	/*
	@tab Footer
	  text style
	@tip The styling for your email's footer text. Make it a color and size that is easy to read.
	*/
		#templateFooter{
			/*@editable*/color:#303030;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:10px;
			/*@editable*/line-height:150%;
		}
	/*
	@tab Footer
	  link style
	@tip The styling for your email's footer hyperlinks. Make them a color that stands out from the rest of your text.
	*/
		#templateFooter a:link,#templateFooter a:visited{
			/*@editable*/color:#404040;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
</style></head>
    <body>
        <center>
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="container">
            	<tr>
                	<td align="center" valign="top">
                        <br>
                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateWrapper">
                            <tr>
                                <td valign="top" width="600" id="preheader">
                                    <div mc:edit="preheader">
                                        <h1>Check out this awesome books!</h1>
                                    </div>
                                    <br>
                                </td>
                            </tr>
                        
                            <tr>
                                <td align="center" valign="top">
                                    <br>
                                    <table border="0" cellpadding="0" cellspacing="0" id="lowerContent">
                                    	<tr>
                                        	<td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <!-- // Begin Repeatable Section \\ -->
                                                    @foreach ($bookInfo as $book)
                                                    <tr mc:repeatable>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:2px solid #d7d5c9;">
                                                                <tr>
                                                                    <td width="35">
                                                                        <br>
                                                                    </td>
                                                                    <td valign="top" width="150" class="repeatableLowerContentLeft">
                                                                        <br>
                                                                        <img src="https://s3.amazonaws.com/freenovels/uploads/{{$book['id']}}.{{$book['coverExt']}}" style="width: 160px; height: 200px;" class="lowerContentImage" mc:edit="repeatable_image">
                                                                        <br>
                                                                    </td>
                                                                    <td width="30">
                                                                        <br>
                                                                    </td>
                                                                    <td align="left" valign="top" width="350" class="repeatableLowerContentRight">
                                                                        <br>
                                                                        <div class="lowerContentCopy" mc:edit="repeatable_content">
                                                                            <span class="subTitle">{{$book['title']}}</span>
                                                                            <br>
                                                                            <p style='float:left;margin-right:15px;'>{{$book['category']}}</p>
                                                                            <p style='float:left;margin-right:15px'>By: {{$book['author']}}</p>
                                                                            <p>${{$book['price']}}</p>
                                                                            <p>{{$book['description']}}</p>
                                                                        </div>
                                                                        <br>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="350">
                                                                            <tr>
                                                                                <td align="center" valign="middle" width="150">
                                                                                    <div class="button">
                                                                                        <br>
                                                                                        <div mc:edit="lower_repeatable_button">
                                                                                            <a href="http://amazon.com/rp/{{$book['amazonAsin']}}" target="_blank">Get It Now!</a>
                                                                                        </div>
                                                                                        <br>
                                                                                    </div>
                                                                                    <br>
                                                                                </td>
                                                                                <td width="10">
                                                                                    <br>
                                                                                </td>
                                
                                                                            </tr>
                                                                        </table>
                                                                        <br>
                                                                    </td>
                                                                    <td width="35">
                                                                        <br>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                                    <!-- // End Repeatable Section \\ -->
                                                    @endforeach
                                                </table>
                                                <br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td id="utility">
                                	<br>
                                    <br>
                               
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3" valign="top" id="templateFooter">
                                    <br>
                                    <div mc:edit="footer">
                                        Copyright &copy; 2015, FreeNovels, All rights reserved.
                                        <br>
                                        <br>
                                    </div>
                                    <br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>