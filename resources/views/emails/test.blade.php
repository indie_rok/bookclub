@extends('templates.emails')


@section('content')

<h1>This new awesome books for you..</h1>

<hr>

@foreach ($bookInfo as $book)
    <img src="{{URL::to('/')}}/img/covers/{{$book['id']}}.{{$book['coverExt']}}" style="width: 160px; height: 200px;">
    <h3>{{$book['title']}}</h3>
    <h4>{{$book['category']}}</h4>
    <h4>${{$book['price']}}</h4>
    <h4>By: {{$book['author']}}</h4>
    <a href="http://amazon.com/rp/{{$book['amazonAsin']}}" style="background-color: #3498db;color:#fff;padding:15px 10px;">Get it</a>
@endforeach
<hr>
@stop