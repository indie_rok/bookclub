@extends('templates.master')

@section('content')

  <div class="jumbotron" style="background: url('{{asset('img/thanks.jpg')}}') no-repeat center center; background-size:cover; color:white">
      <div class="container">
        <h1 class="text-center space100px">Thanks</h1>
      </div>
  </div>

  <article class="container">
	<h2 class="text-center">We need to review your book. We will contact you soon.</h2>
  </article>


@stop