@extends('templates.master')

@section('content')
<div class="jumbotron" style="background: url('{{asset('img/hello.jpg')}}') no-repeat center center; background-size:cover; color:white">
      <div class="container">
        <h1 class="text-center space100px">Welcome</h1>
      </div>
  </div>

<article class="container">
	<h2 class="text-center">You just joined the most awesome eBooks platform ever. We'll keep in touch when we have eBooks for you.</h2>
</article>

@stop