@extends('templates.master')

@section('content')

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron bgIndex">
      <div class="container">
        <h1 class="text-center">Free and Discounted Novels Right to Your Inbox!</h1>
          <a href="{{url('subscriptions/create')}}" class="btn btn-primary btn-lg center-block" style="width:200px">Get started</a>
      </div>
    </div>

    <div class="container text-center" style="font-size:25px">
      <!-- Example row of columns -->
      <div class="row">
        <h1 class="text-center" style="color:#996600">How it Works:</h1>
        <div class="col-md-4">
          <img src="img/hand.jpg" class="center-block">
          <br>
          <p class="text-center">SELECT <br>fiction genres you like</p>
        </div>
        <div class="col-md-4">
          <img src="img/people.jpg" class="center-block">
          <br>
          <p>JOIN <br> the club</p>
       </div>
        <div class="col-md-4">
            <img src="img/book.jpg" class="center-block">
            <br>
          <p>READ <br>high quality free novels</p>
        </div>
      </div>

     <div class="space70px"></div>
         <a href="{{url('subscriptions/create')}}" class="btn btn-primary btn-lg space100px "> Get started </a>
    </div> <!-- /container -->


@stop