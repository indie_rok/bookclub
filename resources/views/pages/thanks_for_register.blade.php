@extends('templates.master')

@section('content')
<div class="jumbotron" style="background: url('{{asset('img/thanks.jpg')}}') no-repeat center center; background-size:cover; color:white">
      <div class="container">
        <h1 class="text-center space100px">Thanks</h1>
      </div>
  </div>

<article class="container">
	<h2 class="text-center">Please check your email. Make sure to add <a href="mailto:email@email.com">email@email.com </a> to your contact list.</h2>
</article>

@stop