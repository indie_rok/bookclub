@extends('templates.master')

@section('content')

  <div class="jumbotron" style="background: url('{{asset('img/shelf.jpg')}}') no-repeat center center; background-size:cover; color:white">
      <div class="container">
        <h1 class="text-center space100px">Select your favorites <br> books categories</h1>
      </div>
  </div>

  <article class="container">

    
  <form method="post" action="{{url('subscriptions/store')}}" id="suscribeForm">

  <div class="row">

      

   <section class="col-md-12 text-center">

      @foreach($activeCategories as $category)
        <input type="button" class="btn bookType" value="{{$category->name}}" data-cat="{{$category->id}}" name="cat">
      @endforeach
        <h4 class="alert alert-danger" id="error" style="display:none">You need to select at least one category</h4>

     <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
     <input type="hidden" name="suscriptions">
     
   </section>

   </div>

   <div class="row text-center">
       <h1>And your email...</h1>
        <input type="email" placeholder="Your email..." class="form-control" name="email" >
  
        <input type="submit" class="btn btn-success" value="Send Me Free Novels">
    </div>

    </div>
 </form>

 </article>
 
  @stop

    @section('scripts')

      <script type="text/javascript">

      $(document).ready(function(){
        $(".bookType").click(function(){
            if ($(this).hasClass("selected")){
              $(this).removeClass("selected");
            }
            else{
              $(this).addClass("selected");
            }
        });
      });

      $("#suscribeForm").on("submit",function(form){

        selectedItems = ($('.selected').length);

        if (selectedItems==0){
          $('#error').show(); 
        }

        var selected = $("#suscribeForm .selected");
        var selectedArray = [];

        $.each(selected,function(key,value){
          selectedArray.push($(value).data("cat"));
        })

        url = (selectedArray.join(","))

        $("[name='suscriptions'").val(url);
        
      });

      
      </script>


    @stop