@extends('templates.master')

@section('content')

  <div class="jumbotron" style="background: url('{{asset('img/shelf.jpg')}}') no-repeat center center; background-size:cover; color:white">
      <div class="container">
        <h1 class="text-center space100px">What's your email address?</h1>
      </div>
  </div>

  <article class="container text-center">
  
      {!!Form::open(['action'=>'SubscriptionsController@update'])!!}
      {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Your email..','id'=>'email'])!!}
      {!!Form::submit('Send',['class'=>'btn btn-primary sub'])!!}
      {!!Form::close()!!}

 </article>
 
  @stop

  @section('scripts')

  <script type="text/javascript">
  email = document.getElementById('email');
  
  $('form').first().on('submit',function(event){
       event.preventDefault();
      if(email.value==""){
        alert('Input the email');
      }
      else{
       document.getElementsByTagName('form')[0].submit();
      }
  })

  
  </script>

  @stop