@extends('templates.master')

@section('content')

  <div class="jumbotron" style="background: url('{{asset('img/shelf.jpg')}}') no-repeat center center; background-size:cover; color:white">
      <div class="container">
        <h1 class="text-center space100px">What categories would you like to hear about?</h1>
      </div>
  </div>

  <article class="container">

    
  <form method="post" action="{{url('subscriptions/editSubscriptions')}}" id="suscribeForm">

  <div class="row">

      

   <section class="col-md-12 text-center">

      @foreach($allCategories as $category)
        <input type="button" class="btn bookType" value="{{$category->name}}" data-cat="{{$category->id}}">
      @endforeach
     <input type="hidden" name="userToken" value="{{$token}}" />
     <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
     <input type="hidden" name="suscriptions">

     <div class="row text-center">
      <br>
        <input type="submit" class="btn btn-success" value="Update My Subscriptions">
    </div>
     
   </section>

   </div>

    </div>
 </form>

 </article>
 
  @stop

    @section('scripts')

      <script type="text/javascript">

      $(document).ready(function(){
        $(".bookType").click(function(){
            if ($(this).hasClass("selected")){
              $(this).removeClass("selected");
            }
            else{
              $(this).addClass("selected");
            }
        });
      });

      $("#suscribeForm").on("submit",function(form){
        var selected = $("#suscribeForm .selected");
        var selectedArray = [];

        $.each(selected,function(key,value){
          selectedArray.push($(value).data("cat"));
        })

        url = (selectedArray.join(","))

        $("[name='suscriptions'").val(url);
        
      });

      
      </script>

    @stop