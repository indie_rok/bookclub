<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ secure_asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ secure_asset('css/jquery-ui.min.css')}}" rel="stylesheet">
    <link href="{{ secure_asset('css/jquery-ui.theme.min.css')}}" rel="stylesheet">
    <link href="{{ secure_asset('css/jquery-ui.structure.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
   <link href="{{ secure_asset('css/home.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

    <body>

    <nav class="navbar navbar-inverse navbar-top ">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand bold" href="/">Free Novel Club</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav pull-right floatoverride">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{url('books/create')}}">Authors</a></li>
            <li><a href="{{url('subscriptions/edit')}}">Edit My Categories</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    @if(Session::has('msj'))
      <section class="alert alert-warning">{{Session::get('msj')}}</section>
    @endif

      @yield('content')

      <hr>

      <footer class="text-center">
        <p>&copy; Book Club 2015</p>
      </footer>

    </body>

   <script type="text/javascript" src="{{ secure_asset('js/jquery-1.11.3.min.js')}}"></script>
   <script type="text/javascript" src="{{ secure_asset('js/jquery-ui.min.js')}}"></script>
   <script type="text/javascript" src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
   <script type="text/javascript" src="{{ secure_asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

      @yield('scripts')

  </body>
</html>
